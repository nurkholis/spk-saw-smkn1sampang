<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::prefix('periode')->group( function() {
    Route::post('/datatable', 'Api\PeriodeController@datatable');
    Route::get('/', 'Api\PeriodeController@get');
    Route::get('/{id}', 'Api\PeriodeController@getById');
    Route::post('/', 'Api\PeriodeController@post');
    Route::put('/{id}', 'Api\PeriodeController@put');
    Route::get('/aktif/{id}', 'Api\PeriodeController@aktif');
});

Route::prefix('kompetensi')->group( function() {
    Route::post('/datatable', 'Api\KompetensiController@datatable');
    Route::get('/', 'Api\KompetensiController@get');
    Route::get('/{id}', 'Api\KompetensiController@getById');
    Route::post('/', 'Api\KompetensiController@post');
    Route::put('/{id}', 'Api\KompetensiController@put');
    Route::get('/aktif/{id}', 'Api\KompetensiController@aktif');
});

Route::prefix('user')->group( function() {
    Route::post('/login', 'Api\UserController@login');
    Route::post('/datatable', 'Api\UserController@datatable');
    Route::get('/', 'Api\UserController@get');
    Route::get('/{id}', 'Api\UserController@getById');
    Route::post('/', 'Api\UserController@post');
    Route::put('/{id}', 'Api\UserController@put');
    Route::put('/pw/{id}', 'Api\UserController@pw');
    Route::get('/reset/{id}', 'Api\UserController@reset');
    Route::delete('/{id}', 'Api\UserController@delete');
});

Route::prefix('kriteria')->group( function() {
    Route::post('/datatable', 'Api\KriteriaController@datatable');
    Route::get('/', 'Api\KriteriaController@get');
    Route::get('/{id}', 'Api\KriteriaController@getById');
    Route::post('/', 'Api\KriteriaController@post');
    Route::put('/{id}', 'Api\KriteriaController@put');
    Route::delete('/{id}', 'Api\KriteriaController@delete');
    Route::post('/prioritas', 'Api\KriteriaController@resetPrioritas');
});

Route::prefix('nilai')->group( function() {
    Route::post('/datatable', 'Api\NilaiController@datatable');
    Route::post('/datatable/guru', 'Api\NilaiController@datatableGuru');
    Route::get('/', 'Api\NilaiController@get');
    Route::get('/{id}', 'Api\NilaiController@getById');
    Route::post('/', 'Api\NilaiController@post');
    Route::put('/{id}', 'Api\NilaiController@put');
    Route::delete('/{id}', 'Api\NilaiController@delete');
});

Route::prefix('plotting')->group( function() {
    Route::post('/datatable', 'Api\PlottingController@datatable');
    Route::get('/user', 'Api\PlottingController@user');
    Route::get('/{id}', 'Api\PlottingController@getById');
    Route::post('/', 'Api\PlottingController@post');
    Route::put('/{id}', 'Api\PlottingController@put');
    Route::delete('/{id}', 'Api\PlottingController@delete');
    Route::delete('', 'Api\PlottingController@reset');
});

Route::prefix('metode')->group( function() {
    Route::post('/bobot', 'Api\MetodeController@bobot');
    Route::get('/hasil/{id}', 'Api\MetodeController@hasil');
});