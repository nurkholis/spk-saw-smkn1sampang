<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/')->group(function () {
    Route::get('/login',  'AuthController@showLoginForm')->name('login');
    Route::post('/login',  'AuthController@login')->name('login.submit');
    Route::get('/logout',  'AuthController@logout')->name('logout');

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/kompetensi', 'HomeController@kompetensi')->name('kompetensi');
    Route::get('/periode', 'HomeController@periode')->name('periode');
    Route::get('/kriteria', 'HomeController@kriteria')->name('kriteria');
    Route::get('/kepsek', 'HomeController@kepsek')->name('kepsek');
    Route::get('/plotting', 'HomeController@plotting')->name('plotting');
    Route::get('/guru', 'HomeController@guru')->name('guru');
    Route::get('/siswa', 'HomeController@siswa')->name('siswa');
    Route::get('/bobot/{id_periode}', 'HomeController@bobot')->name('bobot');
    Route::get('/nilai', 'HomeController@nilai')->name('nilai');
    Route::get('/hasil', 'Api\MetodeController@hasil')->name('hasil');
    Route::get('/profil', 'HomeController@profil')->name('profil');
    Route::get('/pengaturan', 'HomeController@pengaturan')->name('pengaturan');
});