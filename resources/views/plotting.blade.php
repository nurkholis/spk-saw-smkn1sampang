@extends('layouts.app')

@section('content')
    <section class="content-header">
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="card-title">Data Guru</h3>
                                </div>
                                <div class="col-md-3">
                                    <div class="float-right">
                                        <button type="button" onclick="reset()" class="btn btn-danger">
                                            Reset
                                        </button>
                                        <button type="button" onclick="add()" class="btn btn-primary">
                                            Plotting
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nama Guru</th>
                                        <th>Email</th>
                                        <th>Guru Penilai</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@section('modal')
    <div class="modal fade" id="modal-plotting" tabindex="-1" role="dialog" aria-labelledby="exampleStandardModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Form Guru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="form-plotting" action="{{ url('') }}/api/plotting" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Penilai</label>
                            <select class="form-control" name="id_penilai" required>
                                <option value="">Pilih Penilai</option>
                                @foreach ($user as $item)
                                    <option value="{{ $item->id_user }}">{{ $item->nama_user }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Guru</label>
                            <select class="form-control" name="id_user[]" multiple="multiple" required>
                                {{-- @foreach ($user as $item)
                                    <option value="{{ $item->id_user }}">{{ $item->nama_user }}</option>
                                @endforeach --}}
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleStandardModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Form Guru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="form" action="" method="post">
                    <input type="hidden" name="_method" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="nama_user" readonly>
                        </div>
                        <div class="form-group">
                            <label>Penilai</label>
                            <select class="form-control" name="id_penilai" required>
                                <option value="">Pilih Penilai</option>
                                @foreach ($user as $item)
                                    <option value="{{ $item->id_user }}">{{ $item->nama_user }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var id_penilai = '';
        $("select[name='id_penilai']").select2().on('change', function() {
            id_penilai = this.value;
            initSelect2User();
        });
        
        initSelect2User();

        function initSelect2User(){
            console.log(id_penilai);
            $("select[name='id_user[]']").select2({
            allowClear: true,
            placeholder: "Pilih guru",
            ajax: {
                url: "{{ url('') }}/api/plotting/user?id_user_not=" + id_penilai,
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function(term) {
                    return {
                        search: term.term,
                    };
                },
                processResults: function(data) {
                    return {
                        results: $.map(data.data, function(item) {
                            return {
                                text: item.nama_user,
                                id: item.id_user,
                            }
                        })
                    };
                }
            },
        });
        }

        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ url('') }}/api/plotting/datatable?role=3',
                type: 'post',
            },
            columns: [{
                    data: 'nama_user'
                },
                {
                    data: 'email'
                },
                {
                    data: 'nama_penilai'
                },
                {
                    render: function(data, type, row, meta) {
                        var a =
                            '<button type="button" class="btn btn-outline-warning btn-xs me-2" onclick="edit(' +
                            row['id_user'] +
                            ')"><i class="fa fa-edit"></i></button> ';
                        if (row['id_plotting']) {
                            a += '<button type="button" class="btn btn-outline-danger btn-xs" onclick="resetById(' +
                                row['id_plotting'] +
                                ')"><i class="fa fa-redo"></i></button>';
                        }
                        return a;
                    }
                },
            ]
        });

        function resetById(id) {
            if (confirm('Anda yakin ingin mereset data plotting ini?')) {
                $('.dataTables_processing', $('#datatable').closest('.dataTables_wrapper')).show();
                $.ajax({
                    type: 'DELETE',
                    url: "{{ url('') }}/api/plotting/" + id,
                    success: function(r) {
                        $('#datatable').DataTable().ajax.reload();
                    }
                });
            }
        }

        function reset() {
            if (confirm('Anda yakin ingin mereset semua data plotting?')) {
                $('.dataTables_processing', $('#datatable').closest('.dataTables_wrapper')).show();
                $.ajax({
                    type: 'DELETE',
                    url: "{{ url('') }}/api/plotting",
                    success: function(r) {
                        $('#datatable').DataTable().ajax.reload();
                    }
                });
            }
        }

        function add() {
            $("select[name='id_user[]']").val('').trigger('change')
            $('#modal-plotting').modal('show');
        }

        function edit(id) {
            $.ajax({
                url: "{{ url('') }}/api/plotting/" + id,
                success: function(r) {
                    $('#form')[0].reset();
                    $('#modal').modal('show');
                    $("#form").attr("action", "{{ url('') }}/api/plotting/" + id);
                    $("input[name='_method']").val("put");
                    $("input[name='nama_user']").val(r.data.nama_user);
                    if (r.data.plotting) {
                        $("select[name='id_penilai']").val(r.data.plotting.data.id_penilai).trigger(
                            'change');
                    }
                }
            });
        }

        $('#form-plotting').ajaxForm({
            complete: function(response) {
                if (response.status == 200 || response.status == 201) {
                    $('#datatable').DataTable().ajax.reload();
                    $('#modal-plotting').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Data berhasil disimpan'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Toast.fire({
                    icon: 'error',
                    title: JSON.stringify(jqXHR.responseJSON),
                });
            }
        });

        $('#form').ajaxForm({
            complete: function(response) {
                if (response.status == 200 || response.status == 201) {
                    $('#datatable').DataTable().ajax.reload();
                    $('#modal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Data berhasil disimpan'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Toast.fire({
                    icon: 'error',
                    title: JSON.stringify(jqXHR.responseJSON),
                });
            }
        });
    </script>
@endsection
