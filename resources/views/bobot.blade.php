@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ url('') }}/assets/plugins/jquery-ui/jquery-ui.min.css">
@endsection

@section('content')
    <section class="content-header">
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="card-title">Data Kriteria Periode {{ $periode->nama_periode }}</h3>
                                </div>
                                <div class="col-md-3">
                                    <div class="float-right">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <table id="datatable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Periode</th>
                                                <th>Kompetensi</th>
                                                <th>Kode Kriteria</th>
                                                <th>Nama Kriteria</th>
                                                <th>Jenis</th>
                                                <th>Bobot</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <div id="form">
                                        <ul id="sortable" style="list-style-type: none">
                                            @foreach ($kriteria as $item)
                                                <li rel="{{ $item->id_kriteria }}" class="callout callout-info">
                                                    <p>{{ $item->nama_kriteria }}</p>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <small class="float-right">Urutkan kriteria berdasarkan prioritas</small>
                                        <br>
                                        <button type="button" onclick="hitung()" class="btn btn-primary float-right">
                                            Hitung dan Simpan Bobot
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@section('js')
    <script src="{{ url('') }}/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script>
        $("#sortable").sortable();
        $("#sortable").disableSelection();

        function hitung() {
            var list = {};
            var i = 0;
            $('#sortable li').each(function() {
                list['id_kriteria[' + i + ']'] = $(this).attr('rel');
                i++;
            });
            console.log(list);
            $.ajax({
                type: "post",
                url: "{{ url('') }}/api/metode/bobot",
                data: list,
                success: function(response) {
                    $('#datatable').DataTable().ajax.reload();
                    Toast.fire({
                        icon: 'success',
                        title: 'Data berhasil disimpan'
                    });
                }
            });
        }

        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ url('') }}/api/kriteria/datatable?id_periode={{ $periode->id_periode }}',
                type: 'post',
            },
            columns: [{
                    data: 'nama_periode'
                },
                {
                    data: 'nama_kompetensi'
                },
                {
                    data: 'kode_kriteria'
                },
                {
                    data: 'nama_kriteria'
                },
                {
                    data: 'jenis'
                },
                {
                    data: 'bobot'
                },
            ]
        });
    </script>
@endsection
