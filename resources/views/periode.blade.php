@extends('layouts.app')

@section('content')
    <section class="content-header">
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="card-title">Data Periode</h3>
                                </div>
                                <div class="col-md-3">
                                    <button type="button" onclick="add()" class="btn btn-primary float-right">
                                        Tambah
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nama Periode</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@section('modal')
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleStandardModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Form Periode</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="form" action="" method="post">
                    <input type="hidden" name="_method" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Periode</label>
                            <input type="text" class="form-control" name="nama_periode" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ url('') }}/api/periode/datatable',
                type: 'post',
            },
            columns: [{
                    data: 'nama_periode'
                },
                {
                    render: function(data, type, row, meta) {
                        if (row['aktif'] == '1') {
                            return '<button  onclick="aktif(' +
                                row['id_periode'] +
                                ')" type="button" class="btn btn-outline-primary btn-xs">Aktif</button>';
                        } else {
                            return '<button  onclick="aktif(' +
                                row['id_periode'] +
                                ')" type="button" class="btn btn-outline-danger btn-xs">Nonaktif</button>';
                        }
                    }
                },
                {
                    render: function(data, type, row, meta) {
                        return ' <button type="button" class="btn btn-outline-warning btn-xs" onclick="edit(' +
                            row['id_periode'] +
                            ')"><i class="fa fa-edit"></i></button>';
                    }
                },
            ]
        });

        function add() {
            $('#form')[0].reset();
            $('#modal').modal('show');
            $("#form").attr("action", "{{ url('') }}/api/periode");
            $("input[name='_method']").val("post");
        }

        function aktif(id) {
            $('.dataTables_processing', $('#datatable').closest('.dataTables_wrapper')).show();
            $.ajax({
                url: "{{ url('') }}/api/periode/aktif/" + id,
                success: function(r) {
                    $('#datatable').DataTable().ajax.reload();
                }
            });
        }

        function edit(id) {
            $.ajax({
                url: "{{ url('') }}/api/periode/" + id,
                success: function(r) {
                    $('#form')[0].reset();
                    $('#modal').modal('show');
                    $("#form").attr("action", "{{ url('') }}/api/periode/" + id);
                    $("input[name='_method']").val("put");
                    $("input[name='nama_periode']").val(r.data.nama_periode);
                }
            });
        }

        $('#form').ajaxForm({
            complete: function(response) {
                if (response.status == 200 || response.status == 201) {
                    $('#datatable').DataTable().ajax.reload();
                    $('#modal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Data berhasil disimpan'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Toast.fire({
                    icon: 'error',
                    title: JSON.stringify(jqXHR.responseJSON),
                });
            }
        });
    </script>
@endsection
