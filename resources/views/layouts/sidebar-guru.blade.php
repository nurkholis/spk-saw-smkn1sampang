@php
use App\Plotting;
$a = Plotting::where('id_penilai', Auth::guard('web')->user()->id_user)->count('id_plotting');
@endphp
<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-header">GURU</li>
            <li class="nav-item">
                <a href="{{ route('home') }}" class="nav-link {{ request()->is('/') ? 'active' : '' }}">
                    <i class="nav-icon fas fa-home"></i>
                    <p>
                        Dashboard
                    </p>
                </a>
            </li>
            @if ($a > 0)
                <li class="nav-item">
                    <a href="{{ route('nilai') }}" class="nav-link {{ request()->is('nilai') ? 'active' : '' }}">
                        <i class="fas fa-edit nav-icon"></i>
                        <p>Penilaian</p>
                    </a>
                </li>
            @endif
            <li class="nav-item">
                <a href="{{ route('hasil', 1) }}" class="nav-link {{ request()->is('hasil') ? 'active' : '' }}">
                    <i class="fas fa-table nav-icon"></i>
                    <p>Hasil</p>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
