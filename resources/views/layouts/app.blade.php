<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('APP_NAME') }}</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('') }}/assets/plugins/fontawesome-free/css/all.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ url('') }}/assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet"
        href="{{ url('') }}/assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ url('') }}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet"
        href="{{ url('') }}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ url('') }}/assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ url('') }}/assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('') }}/assets/dist/css/adminlte.min.css">
    <link type="text/css" rel="stylesheet" href="{{ url('') }}/assets/dist/jquery.loading-indicator.css" />
    @yield('css')
    <style>
        .m-l-10{
            margin-left: 10px;
        }
    </style>
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i
                            class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('home') }}"
                        class="nav-link"><strong>{{ Auth::guard('web')->user()->nama_user }}</strong></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Navbar Search -->
                <li class="nav-item">
                    <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                        <i class="fas fa-search"></i>
                    </a>
                    <div class="navbar-search-block">
                        <form class="form-inline">
                            <div class="input-group input-group-sm">
                                <input class="form-control form-control-navbar" type="search" placeholder="Search"
                                    aria-label="Search">
                                <div class="input-group-append">
                                    <button class="btn btn-navbar" type="submit">
                                        <i class="fas fa-search"></i>
                                    </button>
                                    <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa fa-cogs"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('profil') }}" class="dropdown-item">
                            <i class="fas fa-user"></i> Profil
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('pengaturan') }}" class="dropdown-item">
                            <i class="fas fa-key"></i> Password
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}" class="dropdown-item">
                            <i class="fas fa-sign-out-alt"></i> Keluar
                        </a>
                        <div class="dropdown-divider"></div>
                    </div>
                </li>
            </ul>

        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{ route('home') }}" class="brand-link">
                <img src="{{ url('') }}/assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">{{ env('APP_NAME') }}</span>
            </a>

            <!-- Sidebar -->
            @if (Auth::guard('web')->user()->role == '1')
                @include('layouts.sidebar-admin')
            @elseif(Auth::guard('web')->user()->role == '2')
                @include('layouts.sidebar-kepsek')
            @else
                @include('layouts.sidebar-guru')
            @endif
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!-- Main content -->
            @yield('content')
            <!-- /.content -->
            @yield('modal')
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 1.0.0
            </div>
            <strong>Copyright &copy; 2021 <a href="{{ route('home') }}">{{ env('APP_NAME') }}</a>.</strong> All
            rights
            reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{ url('') }}/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ url('') }}/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Select2 -->
    <script src="{{ url('') }}/assets/plugins/select2/js/select2.full.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{ url('') }}/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ url('') }}/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ url('') }}/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ url('') }}/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{ url('') }}/assets/plugins/jquery-form/jquery.form.min.js"></script>
    <script src="{{ url('') }}/assets/dist/jquery.loading-indicator.min.js"></script>
    <!-- SweetAlert2 -->
    <script src="{{ url('') }}/assets/plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ url('') }}/assets/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ url('') }}/assets/dist/js/demo.js"></script>
    <!-- Page specific script -->
    <script>
        var loader = $('#form').loadingIndicator({
            useImage: false,
        }).data("loadingIndicator");
        loader.hide();

        $(document).ajaxStart(function() {
            loader.show();
        });
        $(document).ajaxComplete(function() {
            loader.hide();
        });
        var Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        });

        function _hapus(url, callback) {
            Swal.mixin({
                // toast: true,
                // position: 'top-end',
            }).fire({
                title: 'Hapus data?',
                text: "Data akan terhapus dan tidak dapat dikembalikan lagi!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "Ya, Hapus!",
                cancelButtonText: "Tidak, Batal!",
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function(result) {
                if (result['isConfirmed']) {
                    console.log(1);
                    $.ajax({
                        method: "DELETE",
                        url: url,
                        success: function(data, textStatus, xhr) {
                            if (xhr.status == 200) {
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Data berhasil dihapus'
                                });
                                callback();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'Data gagal dihapus'
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            Toast.fire({
                                icon: 'error',
                                title: 'Data gagal dihapus'
                            });
                        }
                    });
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: 'Data batal dihapus'
                    });
                }
            });
        }

        function _reset(url, callback) {
            Swal.mixin({
                // toast: true,
                // position: 'top-end',
            }).fire({
                title: 'Reset password?',
                text: "Pasword akan direset ke '123123123'!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: "Ya, Reset!",
                cancelButtonText: "Tidak, Batal!",
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                buttonsStyling: false
            }).then(function(result) {
                if (result['isConfirmed']) {
                    $('.dataTables_processing', $('#datatable').closest('.dataTables_wrapper')).show();
                    $.ajax({
                        method: "GET",
                        url: url,
                        success: function(data, textStatus, xhr) {
                            if (xhr.status == 200) {
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Data berhasil reset'
                                });
                                callback();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'Data gagal reset'
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            Toast.fire({
                                icon: 'error',
                                title: 'Data gagal reset'
                            });
                        }
                    });
                } else {
                    Toast.fire({
                        icon: 'error',
                        title: 'Data batal reset'
                    });
                }
            });
        }

        $('#modal').on('show.bs.modal', function(e) {
            $(this).removeAttr('tabindex');
        })
    </script>
    @yield('js')
</body>

</html>
