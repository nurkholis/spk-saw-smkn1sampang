<div class="sidebar">

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-header">ADMIN</li>
            <li
                class="nav-item {{ request()->is('kepsek') || request()->is('guru') || request()->is('plotting') ? 'menu-open' : '' }}">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-user"></i>
                    <p>
                        User
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('kepsek') }}"
                            class="nav-link {{ request()->is('kepsek') ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Kepala Sekolah</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('guru') }}" class="nav-link {{ request()->is('guru') ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Guru</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('plotting') }}"
                            class="nav-link {{ request()->is('plotting') ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Plotting</p>
                        </a>
                    </li>
                </ul>
            </li>

            <li
                class="nav-item {{ request()->is('periode') || request()->is('kompetensi') || request()->is('kriteria') || request()->is('nilai') || request()->is('hasil') ? 'menu-open' : '' }}">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-table"></i>
                    <p>
                        Penilaian
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{ route('periode') }}"
                            class="nav-link {{ request()->is('periode') ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Periode</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('kompetensi') }}"
                            class="nav-link {{ request()->is('kompetensi') ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Kompetensi</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('kriteria') }}"
                            class="nav-link {{ request()->is('kriteria') ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Krtiteria</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('nilai') }}"
                            class="nav-link {{ request()->is('nilai') ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Nilai</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('hasil', 1) }}"
                            class="nav-link {{ request()->is('hasil') ? 'active' : '' }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Hasil</p>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
