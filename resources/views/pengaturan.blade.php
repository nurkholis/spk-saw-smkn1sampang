@extends('layouts.app')

@section('content')
    <section class="content-header">
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="card-title">Pengaturan</h3>
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form id="form" action="{{ url('') }}/api/user/pw/{{ Auth::guard('web')->id() }}"
                                method="post">
                                <input type="hidden" name="_method" value="put">
                                <input type="hidden" name="role" value="{{ Auth::guard('web')->user()->role }}">
                                <div class="form-group">
                                    <label>Password Baru</label>
                                    <input type="password" class="form-control" name="password" value="" required>
                                </div>
                                <div class="form-group">
                                    <label>Ulangi Password Baru</label>
                                    <input type="password" class="form-control" name="ulangi" value="" required>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@section('js')
    <script>
        $("#form").submit(function(e) {
            e.preventDefault();
            var password = $("input[name='password']").val();
            var ulangi = $("input[name='ulangi']").val();
            if (password != ulangi) {
                return Toast.fire({
                    icon: 'error',
                    title: 'Isian ulangi password tidak sama',
                });
            }
            $.ajax({
                type: "post",
                url: $("#form").attr('action'),
                data: {
                    password: password,
                    _method: 'put',
                },
                success: function(response, textStatus, xhr) {
                    if (xhr.status == 200 || xhr.status == 201) {
                        Toast.fire({
                            icon: 'success',
                            title: 'Data berhasil disimpan'
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Toast.fire({
                        icon: 'error',
                        title: JSON.stringify(jqXHR.responseJSON),
                    });
                }
            });
        });
    </script>
@endsection
