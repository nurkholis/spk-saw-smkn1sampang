@extends('layouts.app')

@section('css')
@endsection

@section('content')
    <section class="content-header">
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Kompetensi</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <select class="form-control" name="pilih-periode" required>
                                        <option value="">Filter periode</option>
                                        @foreach ($periode as $item)
                                            <option value="{{ $item->id_periode }}">{{ $item->nama_periode }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-8"></div>
                                <div class="col-md-2">
                                    <button type="button" onclick="add()" class="btn btn-primary float-right">
                                        Tambah
                                    </button>
                                </div>
                            </div>
                            <br>
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Periode</th>
                                        <th>Nama Kompetensi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@section('modal')
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleStandardModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Form Kompetensi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="form" action="" method="post">
                    <input type="hidden" name="_method" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Periode</label>
                            <select class="form-control" name="id_periode" required>
                                <option value="">Pilih periode</option>
                                @foreach ($periode as $item)
                                    <option value="{{ $item->id_periode }}">{{ $item->nama_periode }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Kompetensi</label>
                            <input type="text" class="form-control" name="nama_kompetensi" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $("select[name='pilih-periode']").select2({}).on('change', function() {
            $('#datatable').DataTable().ajax.url(
                    '{{ url('') }}/api/kompetensi/datatable?id_periode=' +
                    this.value)
                .load();
        });
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ url('') }}/api/kompetensi/datatable',
                type: 'post',
            },
            columns: [{
                    data: 'nama_periode'
                }, {
                    data: 'nama_kompetensi'
                },
                {
                    render: function(data, type, row, meta) {
                        return ' <button type="button" class="btn btn-outline-warning btn-xs" onclick="edit(' +
                            row['id_kompetensi'] +
                            ')"><i class="fa fa-edit"></i></button>';
                    }
                },
            ]
        });

        function add() {
            $('#form')[0].reset();
            $('#modal').modal('show');
            $("#form").attr("action", "{{ url('') }}/api/kompetensi");
            $("input[name='_method']").val("post");
        }

        function aktif(id) {
            $('.dataTables_processing', $('#datatable').closest('.dataTables_wrapper')).show();
            $.ajax({
                url: "{{ url('') }}/api/kompetensi/aktif/" + id,
                success: function(r) {
                    $('#datatable').DataTable().ajax.reload();
                }
            });
        }

        function edit(id) {
            $.ajax({
                url: "{{ url('') }}/api/kompetensi/" + id,
                success: function(r) {
                    $('#form')[0].reset();
                    $('#modal').modal('show');
                    $("#form").attr("action", "{{ url('') }}/api/kompetensi/" + id);
                    $("input[name='_method']").val("put");
                    $("input[name='nama_kompetensi']").val(r.data.nama_kompetensi);
                    $("select[name='id_periode']").val(r.data.periode.data.id_periode);
                }
            });
        }

        $('#form').ajaxForm({
            complete: function(response) {
                if (response.status == 200 || response.status == 201) {
                    $('#datatable').DataTable().ajax.reload();
                    $('#modal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Data berhasil disimpan'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Toast.fire({
                    icon: 'error',
                    title: JSON.stringify(jqXHR.responseJSON),
                });
            }
        });
    </script>
@endsection
