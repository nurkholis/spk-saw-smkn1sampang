@extends('layouts.app')

@section('content')
    <section class="content-header">
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="card-title">Profil</h3>
                                </div>
                                <div class="col-md-3">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form id="form" action="{{ url('') }}/api/user/{{ Auth::guard('web')->id() }}" method="post">
                                <input type="hidden" name="_method" value="put">
                                <input type="hidden" name="role" value="{{ Auth::guard('web')->user()->role }}">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input type="text" class="form-control" name="nama_user"
                                                value="{{ $user->nama_user }}" required>
                                        </div>
                                        @if (Auth::guard('web')->user()->role != '1')
                                            <div class="form-group">
                                                <label>NIP</label>
                                                <input type="number" class="form-control" name="nip"
                                                    value="{{ $user->nip }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Tempat Lahir</label>
                                                <input type="text" class="form-control" name="tempat_lahir"
                                                    value="{{ $user->tempat_lahir }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Tanggal Lahir</label>
                                                <input type="date" class="form-control" name="tanggal_lahir"
                                                    value="{{ $user->tanggal_lahir }}" required>
                                            </div>
                                        @endif
                                    </div>
                                    @if (Auth::guard('web')->user()->role != '1')
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Pangkat</label>
                                                <input type="text" class="form-control" name="pangkat"
                                                    value="{{ $user->pangkat }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label>TMT Guru</label>
                                                <input type="text" class="form-control" name="tmt_guru"
                                                    value="{{ $user->tmt_guru }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Masa Kerja (Tahun)</label>
                                                <input type="number" class="form-control" name="masa_kerja"
                                                    value="{{ $user->masa_kerja }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Spesialis</label>
                                                <input type="text" class="form-control" name="spesialis"
                                                    value="{{ $user->spesialis }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Program Kehalian</label>
                                                <input type="text" class="form-control" name="program_keahlian"
                                                    value="{{ $user->program_keahlian }}" required>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@section('js')
    <script>
        $('#form').ajaxForm({
            complete: function(response) {
                if (response.status == 200 || response.status == 201) {
                    Toast.fire({
                        icon: 'success',
                        title: 'Data berhasil disimpan'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Toast.fire({
                    icon: 'error',
                    title: JSON.stringify(jqXHR.responseJSON),
                });
            }
        });
    </script>
@endsection
