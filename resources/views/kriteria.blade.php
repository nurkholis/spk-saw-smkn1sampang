@extends('layouts.app')

@section('css')
@endsection

@section('content')
    <section class="content-header">
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Kriteria</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <select class="form-control" name="pilih-periode" required>
                                        <option value="">Filter periode</option>
                                        @foreach ($periode as $item)
                                            <option value="{{ $item->id_periode }}">{{ $item->nama_periode }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-4">
                                    <div class="float-right">
                                        <button type="button" onclick="hitung()" class="btn btn-info">
                                            Hitung Bobot
                                        </button>
                                        <button type="button" onclick="add()" class="btn btn-primary">
                                            Tambah
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Periode</th>
                                        <th>Kompetensi</th>
                                        <th>Kode Kriteria</th>
                                        <th>Nama Kriteria</th>
                                        <th>Jenis</th>
                                        <th>Bobot</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@section('modal')
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleStandardModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Form Kriteria</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="form" action="" method="post">
                    <input type="hidden" name="_method" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Kompetensi</label>
                            <select class="form-control select2" name="id_kompetensi" required>
                                <option value="">Pilih kompetensi</option>
                                @foreach ($kompetensi as $item)
                                    <option value="{{ $item->id_kompetensi }}">{{ $item->nama_kompetensi }} -
                                        {{ $item->nama_periode }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jenis</label>
                            <select class="form-control" name="jenis" required>
                                <option value="">Pilih jenis</option>
                                <option value="benefit">benefit</option>
                                <option value="cost">cost</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Kode Kriteria</label>
                            <input type="text" class="form-control" name="kode_kriteria" required>
                        </div>
                        <div class="form-group">
                            <label>Nama Kriteria</label>
                            <input type="text" class="form-control" name="nama_kriteria" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        var selected_periode = '';
        $("select[name='pilih-periode']").select2({}).on('change', function() {
            selected_periode = this.value;
            $('#datatable').DataTable().ajax.url(
                    '{{ url('') }}/api/kriteria/datatable?id_periode=' +
                    this.value)
                .load();
        });
        $("select[name='id_kompetensi']").select2();
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ url('') }}/api/kriteria/datatable',
                type: 'post',
            },
            columns: [{
                    data: 'nama_periode'
                },
                {
                    data: 'nama_kompetensi'
                },
                {
                    data: 'kode_kriteria'
                },
                {
                    data: 'nama_kriteria'
                },
                {
                    data: 'jenis'
                },
                {
                    data: 'bobot'
                },
                {
                    render: function(data, type, row, meta) {
                        return ' <button type="button" class="btn btn-outline-warning btn-xs" onclick="edit(' +
                            row['id_kriteria'] +
                            ')"><i class="fa fa-edit"></i></button>';
                    }
                },
            ]
        });

        function hitung() {
            if (selected_periode == '') {
                Toast.fire({
                    icon: 'warning',
                    title: 'Anda belum memilih pilih periode',
                });
            } else {
                window.location.href = '{{ url('') }}/bobot/' + selected_periode;
            }
        }

        function add() {
            $('#form')[0].reset();
            $("select[name='id_kompetensi']").val('').trigger('change')
            $('#modal').modal('show');
            $("#form").attr("action", "{{ url('') }}/api/kriteria");
            $("input[name='_method']").val("post");
        }

        function aktif(id) {
            $('.dataTables_processing', $('#datatable').closest('.dataTables_wrapper')).show();
            $.ajax({
                url: "{{ url('') }}/api/kriteria/aktif/" + id,
                success: function(r) {
                    $('#datatable').DataTable().ajax.reload();
                }
            });
        }

        function edit(id) {
            $.ajax({
                url: "{{ url('') }}/api/kriteria/" + id,
                success: function(r) {
                    $("select[name='id_kompetensi']").val('').trigger('change')
                    $('#form')[0].reset();
                    $('#modal').modal('show');
                    $("#form").attr("action", "{{ url('') }}/api/kriteria/" + id);
                    $("input[name='_method']").val("put");
                    $("input[name='nama_kriteria']").val(r.data.nama_kriteria);
                    $("input[name='kode_kriteria']").val(r.data.kode_kriteria);
                    $("select[name='jenis']").val(r.data.jenis);
                    $("select[name='id_kompetensi']").val(r.data.kompetensi.data.id_kompetensi).trigger(
                        'change');
                }
            });
        }

        $('#form').ajaxForm({
            complete: function(response) {
                if (response.status == 200 || response.status == 201) {
                    $('#datatable').DataTable().ajax.reload();
                    $('#modal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Data berhasil disimpan'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Toast.fire({
                    icon: 'error',
                    title: JSON.stringify(jqXHR.responseJSON),
                });
            }
        });
    </script>
@endsection
