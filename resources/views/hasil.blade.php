@extends('layouts.app')

@section('css')
@endsection

@section('content')
    <section class="content-header">
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Nilai Periode <strong>{{ $periode_terpilih->nama_periode }}</strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <select class="form-control" name="pilih-periode" required>
                                        <option value="">Filter periode</option>
                                        @foreach ($periode as $item)
                                            <option value="{{ $item->id_periode }}">{{ $item->nama_periode }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-4">
                                </div>
                            </div>
                            <br>
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Guru</th>
                                        @foreach ($kriteria as $item)
                                            <th>{{ $item->nama_kriteria }}<br>[{{ $item->bobot }}]</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($guru as $_guru)
                                        <tr>
                                            <td>{{ $_guru->nama_user }}</td>
                                            @foreach ($kriteria as $_kriteria)
                                                <td> {{ $_guru['nilai_' . str_replace(' ', '_', strtolower($_kriteria->nama_kriteria))] }}
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>


                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Normalisasi</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="datatable-normalisasi" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Guru</th>
                                        @foreach ($kriteria as $item)
                                            <th>{{ $item->nama_kriteria }}<br>[{{ $item->bobot }}]</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($guru as $_guru)
                                        <tr>
                                            <td>{{ $_guru->nama_user }}</td>
                                            @foreach ($kriteria as $_kriteria)
                                                <td> {{ $_guru['normal_' . str_replace(' ', '_', strtolower($_kriteria->nama_kriteria))] }}
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Ranking</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="datatable-ranking" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Guru</th>
                                                <th>Total</th>
                                                <th>Ranking</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $a = 1;
                                            @endphp
                                            @foreach ($ranking as $_ranking)
                                                <tr>
                                                    <td>{{ $_ranking['nama_user'] }}</td>
                                                    <td>{{ $_ranking['total_bobot'] }}</td>
                                                    <td>{{ $a }}</td>
                                                </tr>
                                                @php
                                                    $a++;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Kesimpulan</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <p>Dari perhitungan nilai menggunakan metode SAW diatas maka, didapat peringkat pertama adalah guru dengan nama <strong>{{ $ranking[0]['nama_user'] }}</strong> dengan total nilai <strong>{{ $ranking[0]['total_bobot'] }}</strong></p>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>


                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@section('modal')
@endsection

@section('js')
    <script>
        $('#datatable').DataTable({});
        $('#datatable-normalisasi').DataTable({});
        // $('#datatable-ranking').DataTable({});

        $("select[name='pilih-periode']").select2({
            allowClear: true,
            placeholder: "Pilih periode",
        }).on('change', function() {
            selected_periode = this.value;
            window.location.href = "{{ route('hasil') }}?id_periode=" + this.value;
        });

        function reloadDataTable() {
            $('#datatable').DataTable().ajax.url(
                    '{{ url('') }}/api/nilai/datatable?id_periode=' +
                    selected_periode)
                .load();
        }
    </script>
@endsection
