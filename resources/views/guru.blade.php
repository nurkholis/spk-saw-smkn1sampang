@extends('layouts.app')

@section('content')
    <section class="content-header">
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="card-title">Data Guru</h3>
                                </div>
                                <div class="col-md-3">
                                    <div class="float-right">
                                        <button type="button" onclick="add()" class="btn btn-primary">
                                            Tambah
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="datatable" class="table table-bordered table-striped table-responsive"
                                style="width: 100% !important">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>NIP</th>
                                        <th>Tetala</th>
                                        <th>Pangkat</th>
                                        <th>TMT</th>
                                        <th>Masker</th>
                                        <th>Spesialis</th>
                                        <th>Keahlian</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@section('modal')
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleStandardModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Form Guru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="form" action="" method="post">
                    <input type="hidden" name="_method" value="">
                    <input type="hidden" name="password" value="123123123">
                    <input type="hidden" name="role" value="3">
                    <div class="modal-body row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="nama_user" required>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" required>
                            </div>
                            <div class="form-group">
                                <label>NIP</label>
                                <input type="number" class="form-control" name="nip" required>
                            </div>
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" class="form-control" name="tempat_lahir" required>
                            </div>
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input type="date" class="form-control" name="tanggal_lahir" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Pangkat</label>
                                <input type="text" class="form-control" name="pangkat" required>
                            </div>
                            <div class="form-group">
                                <label>TMT Guru</label>
                                <input type="date" class="form-control" name="tmt_guru" required>
                            </div>
                            <div class="form-group">
                                <label>Masa Kerja (Tahun)</label>
                                <input type="number" class="form-control" name="masa_kerja" required>
                            </div>
                            <div class="form-group">
                                <label>Spesialis</label>
                                <input type="text" class="form-control" name="spesialis" required>
                            </div>
                            <div class="form-group">
                                <label>Program Kehalian</label>
                                <input type="text" class="form-control" name="program_keahlian" required>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <small>*Password default untuk guru baru adalah <strong>123123123</strong></small>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ url('') }}/api/user/datatable?role=3',
                type: 'post',
            },
            columns: [{
                    data: 'nama_user'
                },
                {
                    data: 'email'
                },
                {
                    data: 'nip'
                },
                {
                    render: function(data, type, row, meta) {
                        return row['tempat_lahir'] + ', ' + row['tanggal_lahir'];
                    }
                },
                {
                    data: 'pangkat'
                },
                {
                    data: 'tmt_guru'
                },
                {
                    data: 'masa_kerja'
                },
                {
                    data: 'spesialis'
                },
                {
                    data: 'program_keahlian'
                },
                {
                    render: function(data, type, row, meta) {
                        return '<button type="button" class="btn btn-outline-warning btn-xs" onclick="edit(' +
                            row['id_user'] +
                            ')"><i class="fa fa-edit"></i></button>' +
                            ' <button type="button" class="btn btn-outline-danger btn-xs" onclick="hapus(' +
                            row['id_user'] +
                            ')"><i class="fa fa-trash"></i></button>' +
                            ' <button type="button" class="btn btn-outline-info btn-xs" onclick="reset(' +
                            row['id_user'] +
                            ')"><i class="fa fa-key"></i></button>';
                    }
                },
            ]
        });

        function add() {
            $('#form')[0].reset();
            $('#modal').modal('show');
            $("#form").attr("action", "{{ url('') }}/api/user");
            $("input[name='_method']").val("post");
        }

        function hapus(id) {
            var url = "{{ url('') }}/api/user/" + id;
            _hapus(url, function() {
                $('#datatable').DataTable().ajax.reload();
            });
        }

        function reset(id) {
            var url = "{{ url('') }}/api/user/reset/" + id;
            _reset(url, function() {
                $('#datatable').DataTable().ajax.reload();
            });
        }

        function aktif(id) {
            $('.dataTables_processing', $('#datatable').closest('.dataTables_wrapper')).show();
            $.ajax({
                url: "{{ url('') }}/api/user/aktif/" + id,
                success: function(r) {
                    $('#datatable').DataTable().ajax.reload();
                }
            });
        }

        function edit(id) {
            $.ajax({
                url: "{{ url('') }}/api/user/" + id,
                success: function(r) {
                    $('#form')[0].reset();
                    $('#modal').modal('show');
                    $("#form").attr("action", "{{ url('') }}/api/user/" + id);
                    $("input[name='_method']").val("put");
                    $("input[name='nama_user']").val(r.data.nama_user);
                    $("input[name='email']").val(r.data.email);
                    $("input[name='nip']").val(r.data.guru.data.nip);
                    $("input[name='tempat_lahir']").val(r.data.guru.data.tempat_lahir);
                    $("input[name='tanggal_lahir']").val(r.data.guru.data.tanggal_lahir);
                    $("input[name='pangkat']").val(r.data.guru.data.pangkat);
                    $("input[name='tmt_guru']").val(r.data.guru.data.tmt_guru);
                    $("input[name='masa_kerja']").val(r.data.guru.data.masa_kerja);
                    $("input[name='spesialis']").val(r.data.guru.data.spesialis);
                    $("input[name='program_keahlian']").val(r.data.guru.data.program_keahlian);
                }
            });
        }

        $('#form').ajaxForm({
            complete: function(response) {
                if (response.status == 200 || response.status == 201) {
                    $('#datatable').DataTable().ajax.reload();
                    $('#modal').modal('hide');
                    Toast.fire({
                        icon: 'success',
                        title: 'Data berhasil disimpan'
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Toast.fire({
                    icon: 'error',
                    title: JSON.stringify(jqXHR.responseJSON),
                });
            }
        });
    </script>
@endsection
