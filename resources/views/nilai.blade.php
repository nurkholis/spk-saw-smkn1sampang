@extends('layouts.app')

@section('css')
@endsection

@section('content')
    <section class="content-header">
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Nilai Periode
                                <strong>{{ $periode_terpilih->nama_periode }}</strong>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <select class="form-control" name="pilih-periode" required>
                                        <option value="">Filter periode</option>
                                        @foreach ($periode as $item)
                                            <option value="{{ $item->id_periode }}">{{ $item->nama_periode }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-4">
                                    {{-- <button type="button" onclick="add()" class="btn btn-primary">
                                        Tambah
                                    </button> --}}
                                </div>
                            </div>
                            <br>
                            <table id="datatable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Guru</th>
                                        @foreach ($kriteria as $item)
                                            <th> {{ $item->nama_kriteria }}</th>
                                        @endforeach
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
@endsection

@section('modal')
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleStandardModalLabel"
        aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Form Nilai</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="form" action="" method="post">
                    <input type="hidden" name="_method" value="">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama Guru</label>
                            <input type="text" class="form-control" name="nama_user" readonly>
                        </div>
                        @foreach ($kriteria as $item)
                            <div class="form-group">
                                <label>Nilai {{ $item->nama_kriteria }}</label>
                                <input type="hidden" class="form-control" name="id_kriteria[]"
                                    value="{{ $item->id_kriteria }}" required>
                                <input type="text" class="form-control decimal"
                                    id="nilai-kriteria-{{ $item->id_kriteria }}" name="nilai[]" value="" required>
                            </div>
                        @endforeach
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('.decimal').keypress(function(evt) {
            return (/^[0-9]*\.?[0-9]*$/).test($(this).val() + evt.key);
        });
        // $('.decimal').keyup(function (e) { 
        //    var val = $(this).val();
        //     if (isNaN(val)) {
        //         val = val.replace(/[^0-9\.]/g, '');
        //         if (val.split('.').length > 2)
        //             val = val.replace(/\.+$/, "");
        //     }
        //     $(this).val(val); 
        // });
        var selected_periode = '{{ $periode_terpilih->id_periode }}';

        $("select[name='pilih-periode']").select2({
            allowClear: true,
            placeholder: "Pilih periode",
        }).on('change', function() {
            selected_periode = this.value;
            window.location.href = "{{ route('nilai') }}?id_periode=" + this.value;
        });

        function reloadDataTable() {
            $('#datatable').DataTable().ajax.url(
                    '{{ url('') }}/api/nilai/datatable?id_penilai={{ $jumlah_plotting > 0 ? $id_user : '' }}&id_periode=' +
                    selected_periode, )
                .load();
        }

        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ url('') }}/api/nilai/datatable?id_penilai={{ $jumlah_plotting > 0 ? $id_user : '' }}&id_periode=' +
                    selected_periode,
                type: 'post',
            },
            columns: [{
                    data: 'nama_user'
                },
                @foreach ($kriteria as $item)
                    {data: '{{ str_replace(' ', '_', strtolower($item->nama_kriteria)) }}'},
                @endforeach {
                    render: function(data, type, row, meta) {
                        return ' <button type="button" class="btn btn-outline-warning btn-xs" onclick="edit(' +
                            row['id_guru'] +
                            ')"><i class="fa fa-edit"></i></button>';
                    }
                },
            ]
        });

        function add() {
            resetForm();
        }

        function edit(id) {
            resetForm();
            $.ajax({
                url: "{{ url('') }}/api/nilai/" + id + "?id_periode=" + selected_periode,
                success: function(r) {
                    $('#modal').modal('show');
                    $("#form").attr("action", "{{ url('') }}/api/nilai/" + id);
                    $("input[name='_method']").val("put");
                    $("input[name='nama_user']").val(r.data.nama_user);
                    nilai = r.data.nilai.data;
                    nilai.forEach(element => {
                        $("#nilai-kriteria-" + element.id_kriteria).attr('value', element.nilai);
                    });
                }
            });
        }

        function resetForm() {
            console.log('reset form');
            @foreach ($kriteria as $item)
                $("#nilai-kriteria-{{ $item->id_kriteria }}").attr('value', '');
            @endforeach
        }

        $('#form').ajaxForm({
            complete: function(response) {
                if (response.status == 200 || response.status == 201) {
                    reloadDataTable();
                    $('#modal').modal('hide');
                    swal({
                        title: "Berhasil!",
                        text: 'data berhasil disimpan',
                        type: "success",
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                swal({
                    title: "Gagal!",
                    text: JSON.stringify(jqXHR.responseJSON),
                    type: "error",
                });
            }
        });
    </script>
@endsection
