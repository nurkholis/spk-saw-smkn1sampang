<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plotting extends Model
{
    protected $table = 'plotting';
    protected $primaryKey = 'id_plotting';
    public $timestamps = false;
    protected $guarded = [];
}
