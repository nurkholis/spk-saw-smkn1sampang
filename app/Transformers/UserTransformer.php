<?php

namespace App\Transformers;

use App\Guru;
use App\Transformers\GuruTransformer;
use App\Plotting;
use App\Transformers\PlottingTransformer;
use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['guru', 'nilai','plotting'];

    public function transform(User $model)
    {
        return [
            'id_user' => $model->id_user,
            'nama_user' => $model->nama_user,
            'email' => $model->email,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }

    public function includeGuru(User $model)
    {
        $data = Guru::where('guru.id_user', '=', $model->id_user)->first();
        return $data ? $this->item($data, new GuruTransformer) : null;
    }

    public function includePlotting(User $model)
    {
        $data = Plotting::where('plotting.id_user', '=', $model->id_user)->first();
        return $data ? $this->item($data, new PlottingTransformer) : null;
    }
}
