<?php

namespace App\Transformers;

use App\Nilai;
use League\Fractal\TransformerAbstract;

class NilaiTransformer extends TransformerAbstract
{
    protected $availableIncludes = [];

    public function transform(Nilai $model)
    {
        return [
            'id_guru' => $model->id_guru,
            'id_kriteria' => $model->id_kriteria,
            'nilai' => $model->nilai,
        ];
    }
}