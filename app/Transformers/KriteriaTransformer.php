<?php

namespace App\Transformers;

use App\Kriteria;
use League\Fractal\TransformerAbstract;
use App\Kompetensi;
use App\Transformers\KompetensiTransformer;

class KriteriaTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['kompetensi'];

    public function transform(Kriteria $model)
    {
        return [
            'id_kriteria' => $model->id_kriteria,
            'id_kompetensi' => $model->id_kompetensi,
            'kode_kriteria' => $model->kode_kriteria,
            'nama_kriteria' => $model->nama_kriteria,
            'jenis' => $model->jenis,
            'prioritas' => $model->prioritas,
            'bobot' => $model->bobot,
        ];
    }

    public function includeKompetensi(Kriteria $model)
    {
        $data = Kompetensi::find($model->id_kompetensi);
        return $this->item($data, new KompetensiTransformer);
    }
}