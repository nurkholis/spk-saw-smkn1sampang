<?php

namespace App\Transformers;

use App\Guru;
use App\Nilai;
use League\Fractal\TransformerAbstract;
use App\Transformers\NilaiTransformer;

class GuruTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['nilai'];
    private $id_periode;

    public function __construct($id_periode = null)
    {
        $this->id_periode = $id_periode;
    }

    public function transform(Guru $model)
    {
        return [
            'id_guru' => $model->id_guru,
            'id_user' => $model->id_user,
            'nama_user' => $model->nama_user,
            'id_penilai' => $model->id_penilai,
            'nip' => $model->nip,
            'tempat_lahir' => $model->tempat_lahir,
            'tanggal_lahir' => $model->tanggal_lahir,
            'pangkat' => $model->pangkat,
            'tmt_guru' => $model->tmt_guru,
            'masa_kerja' => $model->masa_kerja,
            'spesialis' => $model->spesialis,
            'program_keahlian' => $model->program_keahlian,
        ];
    }

    public function includeNilai(Guru $model)
    {
        $data = Nilai::join('kriteria', 'kriteria.id_kriteria', '=', 'nilai.id_kriteria')
            ->join('kompetensi', 'kompetensi.id_kompetensi', '=', 'kriteria.id_kompetensi')
            ->where('nilai.id_guru', $model->id_guru)
            ->where('kompetensi.id_periode', $this->id_periode)
            ->get();
        return $this->collection($data, new NilaiTransformer);
    }
}
