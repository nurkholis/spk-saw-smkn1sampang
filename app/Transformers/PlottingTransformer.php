<?php

namespace App\Transformers;

use App\Plotting;
use League\Fractal\TransformerAbstract;

class PlottingTransformer extends TransformerAbstract
{
    protected $availableIncludes = [];

    public function transform(Plotting $model)
    {
        return [
            'id_plotting' => $model->id_plotting,
            'id_user' => $model->id_user,
            'id_penilai' => $model->id_penilai,
        ];
    }
}