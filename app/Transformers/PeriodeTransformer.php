<?php

namespace App\Transformers;

use App\Periode;
use League\Fractal\TransformerAbstract;

class PeriodeTransformer extends TransformerAbstract
{
    protected $availableIncludes = [];

    public function transform(Periode $model)
    {
        return [
            'id_periode' => $model->id_periode,
            'nama_periode' => $model->nama_periode,
            'aktif' => $model->aktif,
        ];
    }
}