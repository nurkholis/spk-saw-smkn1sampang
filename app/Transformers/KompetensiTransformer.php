<?php

namespace App\Transformers;

use App\Kompetensi;
use App\Periode;
use App\Transformers\PeriodeTransformer;
use League\Fractal\TransformerAbstract;

class KompetensiTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['periode'];

    public function transform(Kompetensi $model)
    {
        return [
            'id_kompetensi' => $model->id_kompetensi,
            'id_periode' => $model->id_periode,
            'nama_kompetensi' => $model->nama_kompetensi,
        ];
    }

    public function includePeriode(Kompetensi $model)
    {
        $data = Periode::find($model->id_periode);
        return $this->item($data, new PeriodeTransformer);
    }
}
