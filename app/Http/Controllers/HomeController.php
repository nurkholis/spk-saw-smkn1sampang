<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\kompetensi;
use App\Kriteria;
use App\Periode;
use App\Plotting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    public function allowUser($role_list = [])
    {
        $auth = false;
        $role = Auth::guard('web')->user()->role;
        foreach ($role_list as $key => $value) {
            if ($role == $value) {
                $auth = true;
            }
        }
        return $auth;
    }

    public function index()
    {
        $p = Periode::count('id_periode');
        $k = Kompetensi::count('id_kompetensi');
        $kt = Kriteria::count('id_kriteria');
        $g = User::where('role', '3')->count('id_user');
        return view('home', [
            'p' => $p,
            'k' => $k,
            'kt' => $kt,
            'g' => $g,
        ]);
    }

    public function periode()
    {if (!$this->allowUser([1, 2])) {
        return redirect('/login');
    }
        return view('periode');
    }

    public function kompetensi()
    {if (!$this->allowUser([1, 2])) {
        return redirect('/login');
    }
        $periode = Periode::all();
        return view('kompetensi', [
            'periode' => $periode,
        ]);
    }

    public function kriteria()
    {
        if (!$this->allowUser([1, 2])) {
            return redirect('/login');
        }
        $kompetensi = Kompetensi::join('periode', 'periode.id_periode', '=', 'kompetensi.id_periode')->get();
        $periode = Periode::all();
        return view('kriteria', [
            'kompetensi' => $kompetensi,
            'periode' => $periode,
        ]);
    }

    public function kepsek()
    {
        if (!$this->allowUser([1])) {
            return redirect('/login');
        }
        return view('kepsek');
    }

    public function guru()
    {
        if (!$this->allowUser([1, 2])) {
            return redirect('/login');
        }
        return view('guru');
    }

    public function bobot(Request $request, $id_periode)
    {
        if (!$this->allowUser([1, 2])) {
            return redirect('/login');
        }
        $kriteria = Kriteria::join('kompetensi', 'kompetensi.id_kompetensi', '=', 'kriteria.id_kompetensi')
            ->where('kompetensi.id_periode', $id_periode)
            ->orderby('kriteria.bobot', 'desc')
            ->get();
        $periode = Periode::find($id_periode);
        return view('bobot', [
            'kriteria' => $kriteria,
            'periode' => $periode,
        ]);
    }

    public function plotting()
    {
        if (!$this->allowUser([1, 2])) {
            return redirect('/login');
        }
        $user = User::where('role', '3')->get();
        return view('plotting', [
            'user' => $user,
        ]);
    }

    public function profil()
    {
        $user = User::leftJoin('guru', 'guru.id_user', '=', 'user.id_user')->find(Auth::guard('web')->id());
        return view('profil', [
            'user' => $user,
        ]);
    }

    public function pengaturan()
    {
        $user = User::leftJoin('guru', 'guru.id_user', '=', 'user.id_user')->find(Auth::guard('web')->id());
        return view('pengaturan', [
            'user' => $user,
        ]);
    }

    public function nilai(Request $request)
    {
        $jumlah_plotting = Plotting::where('id_penilai', Auth::guard('web')->user()->id_user)->count('id_plotting');
        if (Auth::guard('web')->user()->role == '3') {
            if ($jumlah_plotting == 0) {
                return redirect('/login');
            }
        }

        $periode = Periode::all();
        if ($request->id_periode) {
            $periode_terpilih = Periode::where('id_periode', $request->id_periode)->first();
        } else {
            $periode_terpilih = Periode::where('aktif', '1')->first();
        }
        $kriteria = Kriteria::join('kompetensi', 'kompetensi.id_kompetensi', '=', 'kriteria.id_kompetensi')
            ->where('kompetensi.id_periode', $periode_terpilih->id_periode)
            ->orderby('kriteria.bobot', 'desc')
            ->get();
        // cek user sebagai guru penilai

        return view('nilai', [
            'periode' => $periode,
            'periode_terpilih' => $periode_terpilih,
            'kriteria' => $kriteria,
            'jumlah_plotting' => $jumlah_plotting,
            'id_user' => Auth::guard('web')->user()->id_user,
        ]);
    }
}
