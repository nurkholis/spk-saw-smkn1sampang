<?php

namespace App\Http\Controllers\Api;

use App\Guru;
use App\Http\Controllers\Controller;
use App\Kriteria;
use App\Nilai;
use App\Periode;
use Illuminate\Http\Request;

class MetodeController extends Controller
{

    public function bobot(Request $request)
    {
        $list = [];
        foreach ($request->id_kriteria as $key => $value) {
            array_push($list, $value);
        }
        $hasil = $this->roc($list);
        foreach ($hasil as $key => $value) {
            Kriteria::where('id_kriteria', $value['key'])->update(['bobot' => $value['bobot']]);
        }
        return response()->json(['data' => $hasil], 200);
    }

    public function hasil(Request $request)
    {
        if ($request->id_periode) {
            $periode_terpilih = Periode::where('id_periode', $request->id_periode)->first();
        } else {
            $periode_terpilih = Periode::where('aktif', '1')->first();
        }
        $guru = Guru::select('guru.*', 'user.nama_user')
            ->join('user', 'user.id_user', '=', 'guru.id_user')
            ->where('role', '3')
            ->get();

        $kriteria = Kriteria::join('kompetensi', 'kompetensi.id_kompetensi', '=', 'kriteria.id_kompetensi')
            ->where('kompetensi.id_periode', $periode_terpilih->id_periode)
            ->get();
        // proses mencari nilai terbesar dan terkecil masing-masing kriteria
        foreach ($kriteria as $key => $value) {
            $min = Nilai::where('nilai.id_kriteria', $value->id_kriteria)->min('nilai');
            $max = Nilai::where('nilai.id_kriteria', $value->id_kriteria)->max('nilai');
            $kriteria[$key]['min'] = $min;
            $kriteria[$key]['max'] = $max;
        }
        // dd($kriteria);
        // proses normalisasi
        $ranking = [];
        foreach ($guru as $a => $_guru) {
            $total_bobot = 0;
            foreach ($kriteria as $b => $_kriteria) {
                $n = Nilai::where('id_guru', $_guru->id_guru)
                    ->where('id_kriteria', $_kriteria->id_kriteria)->first();
                $nilai = $n ? $n->nilai : '';
                $guru[$a]['nilai_' . str_replace(' ', '_', strtolower($_kriteria->nama_kriteria))] = $nilai;
                $normal = 0;
                if ($_kriteria->jenis == 'benefit') {
                    $normal = (double) $nilai / $_kriteria->max;
                } else {
                    $normal = $_kriteria->min != 0 ? (double) $nilai / $_kriteria->min : null;
                }
                $guru[$a]['normal_' . str_replace(' ', '_', strtolower($_kriteria->nama_kriteria))] = round($normal, 3);
                $guru[$a]['bobot_' . str_replace(' ', '_', strtolower($_kriteria->nama_kriteria))] = $normal * $_kriteria->bobot;
                $total_bobot = $total_bobot + ($normal * $_kriteria->bobot);

                // echo $_kriteria['kode_kriteria'];
                // if ($_kriteria['kode_kriteria'] == 'C5') {
                //     echo (double) $nilai;
                // }
            }
            $guru[$a]['total_bobot'] = $total_bobot;
            array_push($ranking, [
                'nama_user' => $_guru->nama_user,
                'total_bobot' => round($total_bobot, 3),
            ]);
        }
        // dd($guru[1]);
        // dd($kriteria);
        // proses peangkingan
        usort($ranking, function ($a, $b) {
            return strcmp($a['total_bobot'], $b['total_bobot']);
        });
        $ranking = array_reverse($ranking);
        // dd($ranking);
        // dd($guru[0]);
        $kriteria = Kriteria::join('kompetensi', 'kompetensi.id_kompetensi', '=', 'kriteria.id_kompetensi')
            ->where('kompetensi.id_periode', $periode_terpilih->id_periode)
            ->orderby('kriteria.bobot', 'desc')
            ->get();
        $periode = Periode::get();
        return view('hasil', [
            'guru' => $guru,
            'periode' => $periode,
            'kriteria' => $kriteria,
            'ranking' => $ranking,
            'periode_terpilih' => $periode_terpilih,
        ]);
    }

    public function roc($list = [])
    {
        $b = 1;
        $n = count($list);
        $roc = [];
        for ($i = 1; $i < $n + 1; $i++) {
            $pembilang = 0;
            $x = 0;
            $proses = '';
            for ($b = $i; $b < $n + 1; $b++) {
                $proses = $proses . '1' . '/' . $b . ($b == $n ? ' = ' : ' + ');
                $pembilang = $pembilang + (1 / $b);
            }
            $x = round($pembilang / $n, 3);
            $proses = $proses . round($pembilang, 3) . '/' . $n . ' = ' . $x . '';
            array_push($roc, [
                'key' => $list[$i - 1],
                'bobot' => $x,
                'proses' => $proses,
            ]);
        }
        return $roc;
    }
}
