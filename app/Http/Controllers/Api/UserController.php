<?php

namespace App\Http\Controllers\Api;

use App\Guru;
use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use App\User;
use Auth;
use DataTables;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:6',
        ]);
        $credential = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if (Auth::guard('web')->attempt($credential)) {
            $data = User::find(Auth::guard('web')->id());
            return fractal()
                ->item($data)
                ->transformWith(new UserTransformer)
                ->addMeta([
                    'token' => $data->api_token,
                ])
                ->toArray();
        } else {
            return response()->json(['error' => 'email atau password salah'], 401);
        }
    }

    public function datatable(Request $request)
    {
        $data = User::select('user.id_user', 'user.nama_user', 'user.email', 'user.created_at', 'user.updated_at', 'user.role', 'guru.*')
            ->leftJoin('guru', 'guru.id_user', '=', 'user.id_user');
        if ($request->exists('role')) {
            $data = $data->where('user.role', '=', $request->role);
            $param['role'] = $request->role;
        }
        $data = $data->get();
        return DataTables::of($data)
            ->make(true);
    }

    public function get(Request $request)
    {
        $limit = 10;
        $param = [];
        $data = User::orderBy('id_user', 'desc');
        if ($request->exists('nama_user')) {
            $data = $data->where('nama_user', 'LIKE', "%" . $request->nama_user . "%");
            $param['nama_user'] = $request->nama_user;
        }
        if ($request->exists('nip')) {
            $data = $data->where('nip', '=', $request->nip);
            $param['nip'] = $request->nip;
        }
        if ($request->exists('role')) {
            $data = $data->where('role', '=', $request->role);
            $param['role'] = $request->role;
        }
        if ($request->exists('limit')) {
            $limit = $request->limit;
            $param['limit'] = $request->limit;
        }
        $data = $data->paginate($limit)
            ->appends($param);
        return fractal()
            ->collection($data)
            ->includeGuru()
            ->transformWith(new UserTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->toArray();
    }

    public function getById(Request $request, $id)
    {
        $data = User::find($id);
        return fractal()
            ->item($data)
            ->includeGuru()
            ->transformWith(new UserTransformer)
            ->toArray();
    }

    public function post(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:user',
            'nama_user' => 'required|min:3|max:40',
            'password' => 'required|min:6',
            'role' => 'required|in:1,2,3',
            'nip' => 'required|numeric',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date_format:Y-m-d',
            'pangkat' => 'required',
            'tmt_guru' => 'required',
            'masa_kerja' => 'required',
            'spesialis' => 'required',
            'program_keahlian' => 'required',
        ]);

        $data = User::create([
            'id_user' => null,
            'email' => $request->email,
            'nama_user' => $request->nama_user,
            'role' => $request->role,
            'password' => bcrypt($request->password),
            'api_token' => bcrypt($request->password),
        ]);

        if($request != '1')
        {
            Guru::create([
                'id_user' => $data->id_user,
                'nip' => $request->nip,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'pangkat' => $request->pangkat,
                'tmt_guru' => $request->tmt_guru,
                'masa_kerja' => $request->masa_kerja,
                'spesialis' => $request->spesialis,
                'program_keahlian' => $request->program_keahlian,
            ]);
        }

        return fractal()
            ->item($data)
            ->includeGuru()
            ->transformWith(new UserTransformer)
            ->addMeta([
                'token' => $data->api_token,
            ])
            ->toArray();
    }

    public function put(Request $request, $id)
    {
        $this->validate($request, [
            'nama_user' => $request->nama_user != "" ? 'required|min:3|max:40' : '',
            'role' => $request->role != "" ? 'required|in:1,2,3' : '',
            'nip' => $request->nip != "" ? 'required|numeric' : '',
            'tempat_lahir' => $request->tempat_lahir != "" ? 'required' : '',
            'tanggal_lahir' => $request->tanggal_lahir != "" ? 'required|date_format:Y-m-d' : '',
            'pangkat' => $request->pangkat != "" ? 'required' : '',
            'tmt_guru' => $request->tmt_guru != "" ? 'required' : '',
            'masa_kerja' => $request->masa_kerja != "" ? 'required' : '',
            'spesialis' => $request->spesialis != "" ? 'required' : '',
            'program_keahlian' => $request->program_keahlian != "" ? 'required' : '',
        ]);

        $data = User::find($id);
        $data->nama_user = $request->get('nama_user', $data->nama_user);
        $data->email = $request->get('email', $data->email);
        $data->role = $request->get('role', $data->role);
        $data->save();

        if ($request->role != '1') {
            $guru = Guru::where('id_user', $data->id_user)->first();
            $guru->nip = $request->get('nip', $data->nip);
            $guru->tempat_lahir = $request->get('tempat_lahir', $data->tempat_lahir);
            $guru->tanggal_lahir = $request->get('tanggal_lahir', $data->tanggal_lahir);
            $guru->pangkat = $request->get('pangkat', $data->pangkat);
            $guru->tmt_guru = $request->get('tmt_guru', $data->tmt_guru);
            $guru->masa_kerja = $request->get('masa_kerja', $data->masa_kerja);
            $guru->spesialis = $request->get('spesialis', $data->spesialis);
            $guru->program_keahlian = $request->get('program_keahlian', $data->program_keahlian);
            $guru->save();
        }
        return fractal()
            ->item($data)
            ->includeGuru()
            ->transformWith(new UserTransformer)
            ->addMeta([
                'token' => $data->api_token,
            ])
            ->toArray();
    }

    public function pw(Request $request, $id)
    {
        $data = User::find($id);
        $data->password = bcrypt($request->password);
        $data->save();
        return fractal()
            ->item($data)
            ->includeGuru()
            ->transformWith(new UserTransformer)
            ->addMeta([
                'token' => $data->api_token,
            ])
            ->toArray();
    }

    public function reset(Request $request, $id)
    {
        $data = User::find($id);
        $data->password = bcrypt('123123123');
        $data->save();
        return fractal()
            ->item($data)
            ->includeGuru()
            ->transformWith(new UserTransformer)
            ->addMeta([
                'token' => $data->api_token,
            ])
            ->toArray();
    }

    public function plotting(Request $request)
    {
        $data = User::select('user.id_user', 'user.nama_user', 'user.email', 'user.created_at', 'user.updated_at', 'user.role', 'guru.*')
            ->leftJoin('guru', 'guru.id_user', '=', 'user.id_user');
        if ($request->exists('role')) {
            $data = $data->where('user.role', '=', $request->role);
            $param['role'] = $request->role;
        }
        $data = $data->get();
        return DataTables::of($data)
            ->make(true);
    }
}
