<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Kompetensi;
use App\Transformers\KompetensiTransformer;
use DataTables;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class KompetensiController extends Controller
{
    public function datatable(Request $request)
    {
        $data = Kompetensi::join('periode', 'periode.id_periode', '=', 'kompetensi.id_periode');
        if ($request->exists('id_periode')) {
            $data = $data->where('periode.id_periode', '=', $request->id_periode);
            $param['id_periode'] = $request->id_periode;
        }
        $data = $data->get();
        return DataTables::of($data)
            ->make(true);
    }

    public function get(Request $request)
    {
        $limit = 10;
        $param = [];
        $data = Kompetensi::orderBy('id_kompetensi', 'asc');
        if ($request->exists('id_peserta')) {
            $data = $data->where('id_peserta', '=', $request->id_peserta);
            $param['id_peserta'] = $request->id_peserta;
        }
        if ($request->exists('id_periode')) {
            $data = $data->where('id_periode', '=', $request->id_periode);
            $param['id_periode'] = $request->id_periode;
        }
        if ($request->exists('limit')) {
            $limit = $request->limit;
            $param['limit'] = $request->limit;
        }
        $data = $data->paginate($limit)
            ->appends($param);

        return fractal()
            ->collection($data)
            ->includePeriode()
            ->transformWith(new KompetensiTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->toArray();
    }

    public function getById(Request $request, $id)
    {
        $data = Kompetensi::find($id);
        return fractal()
            ->item($data)
            ->includePeriode()
            ->transformWith(new KompetensiTransformer)
            ->toArray();
    }

    public function post(Request $request)
    {
        $this->validate($request, [
            'id_periode' => 'required|exists:periode,id_periode',
            'nama_kompetensi' => 'required',
        ]);
        Kompetensi::create([
            'id_periode' => $request->id_periode,
            'nama_kompetensi' => $request->nama_kompetensi,
        ]);
        return response()->json(['message' => 'created'], 201);
    }

    public function put(Request $request, $id)
    {
        $this->validate($request, [
            'id_periode' => 'required|exists:periode,id_periode',
            'nama_kompetensi' => 'required',
        ]);
        $data = Kompetensi::find($id);
        $data->id_periode = $request->get('id_periode', $data->id_periode);
        $data->nama_kompetensi = $request->get('nama_kompetensi', $data->nama_kompetensi);
        $data->save();
        return response()->json(['message' => 'updated'], 200);
    }

    public function delete(Request $request, $id)
    {
        Kompetensi::find($id)->delete();
        return response()->json(['message' => 'deleted'], 200);
    }
}
