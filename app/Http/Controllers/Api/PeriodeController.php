<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Periode;
use App\Transformers\PeriodeTransformer;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use DataTables;

class PeriodeController extends Controller
{
    public function datatable()
    {
        $data = Periode::get();
        return DataTables::of($data)
            ->make(true);
    }
    
    public function get(Request $request)
    {
        $limit = 10;
        $param = [];
        $data = Periode::orderBy('id_periode', 'asc');
        if ($request->exists('aktif')) {
            $data = $data->where('aktif', '=', $request->aktif);
            $param['aktif'] = $request->aktif;
        }
        if ($request->exists('search')) {
            $data = $data->where('nama_periode', 'like', '%' . $request->search. '%');
            $param['search'] = $request->search;
        }
        if ($request->exists('limit')) {
            $limit = $request->limit;
            $param['limit'] = $request->limit;
        }
        $data = $data->paginate($limit)
            ->appends($param);

        return fractal()
            ->collection($data)
            ->transformWith(new PeriodeTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->toArray();
    }

    public function getById(Request $request, $id)
    {
        $data = Periode::find($id);
        return fractal()
            ->item($data)
            ->transformWith(new PeriodeTransformer)
            ->toArray();
    }

    public function post(Request $request)
    {
        $this->validate($request, [
            'nama_periode' => 'required',
        ]);
        Periode::create([
            'nama_periode' => $request->nama_periode,
            'aktif' => '0',
        ]);
        return response()->json(['message' => 'created'], 201);
    }

    public function put(Request $request, $id)
    {
        $this->validate($request, [
            'nama_periode' => 'required',
        ]);
        $data = Periode::find($id);
        $data->nama_periode = $request->get('nama_periode', $data->nama_periode);
        $data->save();
        return response()->json(['message' => 'updated'], 200);
    }

    public function aktif(Request $request, $id)
    {
        Periode::where('id_periode', '!=', $id)->update(['aktif' => '0']);
        Periode::where('id_periode', '=', $id)->update(['aktif' => '1']);
        return response()->json(['message' => 'updated'], 200);
    }
}
