<?php

namespace App\Http\Controllers\Api;

use App\Guru;
use App\Http\Controllers\Controller;
use App\Plotting;
use App\Transformers\UserTransformer;
use App\User;
use DataTables;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class PlottingController extends Controller
{

    public function datatable(Request $request)
    {
        $data = User::select('plotting.*', 'user.*', 'penilai.nama_user as nama_penilai', 'penilai.id_user as id_penilai')
            ->leftJoin('plotting', 'plotting.id_user', '=', 'user.id_user')
            ->leftJoin('user as penilai', 'penilai.id_user', '=', 'plotting.id_penilai')
            ->where('user.role', '3')
            ->get();

        return DataTables::of($data)
            ->make(true);
    }

    public function user(Request $request)
    {
        $data = User::select('plotting.*', 'user.*', 'penilai.nama_user as nama_penilai', 'penilai.id_user as id_penilai')
            ->leftJoin('plotting', 'plotting.id_user', '=', 'user.id_user')
            ->leftJoin('user as penilai', 'penilai.id_user', '=', 'plotting.id_penilai')
            ->where('user.role', '3')
            ->whereNull('penilai.nama_user');

        if ($request->exists('id_user_not')) {
            $data = $data->where('user.id_user', '!=', $request->id_user_not);
            $param['id_user_not'] = $request->id_user_not;
        }
        $data = $data->get();

        return fractal()
            ->collection($data)
            ->includeGuru()
            ->transformWith(new UserTransformer)
            ->toArray();
    }

    public function getById(Request $request, $id_user)
    {
        $data = User::find($id_user);
        return fractal()
            ->item($data)
            ->includePlotting()
            ->transformWith(new UserTransformer)
            ->toArray();
    }

    public function put(Request $request, $id_user)
    {
        $this->validate($request, [
            'id_penilai' => 'required|exists:user,id_user',
        ]);
        if ($id_user == $request->id_penilai) {
            return response()->json(['message' => 'pilih penilai lainnya'], 500);
        }
        $data = Plotting::where('id_user', $id_user)->count();
        if ($data) {
            Plotting::where('id_user', $id_user)->update(['id_penilai' => $request->id_penilai]);
        } else {
            Plotting::create(['id_user' => $id_user, 'id_penilai' => $request->id_penilai]);
        }
        return response()->json(['message' => 'updated'], 200);
    }

    public function post(Request $request)
    {
        $this->validate($request, [
            'id_penilai' => 'required|exists:user,id_user',
        ]);
        foreach ($request->id_user as $key => $value) {
            $data = Plotting::where('id_user', $value)->count();
            if ($data) {
                Plotting::where('id_user', $value)->update(['id_penilai' => $request->id_penilai]);
            } else {
                Plotting::create(['id_user' => $value, 'id_penilai' => $request->id_penilai]);
            }
        }
        return response()->json(['message' => 'updated'], 200);
    }

    public function delete($id)
    {
        Plotting::find($id)->delete();
        return response()->json(['message' => 'deleted'], 200);
    }

    public function reset()
    {
        Plotting::whereNotNull('id_plotting')->delete();
        return response()->json(['message' => 'reseted'], 200);
    }
}
