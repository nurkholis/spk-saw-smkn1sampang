<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Kriteria;
use App\Transformers\KriteriaTransformer;
use DataTables;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class KriteriaController extends Controller
{
    public function datatable(Request $request)
    {
        $data = Kriteria::join('kompetensi', 'kompetensi.id_kompetensi', '=', 'kriteria.id_kompetensi')
            ->join('periode', 'periode.id_periode', '=', 'kompetensi.id_periode');
        if ($request->exists('id_periode')) {
            $data = $data->where('periode.id_periode', '=', $request->id_periode);
            $param['id_periode'] = $request->id_periode;
        }
        $data = $data->get();
        return DataTables::of($data)
            ->make(true);
    }

    public function get(Request $request)
    {
        $limit = 10;
        $param = [];
        $data = Kriteria::orderBy('id_kriteria', 'asc');
        if ($request->exists('id_kompetensi')) {
            $data = $data->where('id_kompetensi', '=', $request->id_kompetensi);
            $param['id_kompetensi'] = $request->id_kompetensi;
        }
        if ($request->exists('limit')) {
            $limit = $request->limit;
            $param['limit'] = $request->limit;
        }
        $data = $data->paginate($limit)
            ->appends($param);

        return fractal()
            ->collection($data)
            ->parseIncludes('kompetensi.periode')
            ->transformWith(new KriteriaTransformer)
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->toArray();
    }

    public function getById(Request $request, $id)
    {
        $data = Kriteria::find($id);
        return fractal()
            ->item($data)
            ->parseIncludes('kompetensi.periode')
            ->transformWith(new KriteriaTransformer)
            ->toArray();
    }

    public function post(Request $request)
    {
        $this->validate($request, [
            'id_kompetensi' => 'required|exists:kompetensi,id_kompetensi',
            'kode_kriteria' => 'required|unique:kriteria|max:20',
            'nama_kriteria' => 'required',
            'jenis' => 'required|in:benefit,cost',
        ]);
        Kriteria::create([
            'id_kompetensi' => $request->id_kompetensi,
            'kode_kriteria' => $request->kode_kriteria,
            'nama_kriteria' => $request->nama_kriteria,
            'jenis' => $request->jenis,
            'prioritas' => $request->prioritas,
        ]);
        return response()->json(['message' => 'created'], 201);
    }

    public function put(Request $request, $id)
    {
        $this->validate($request, [
            'id_kompetensi' => $request->id_kompetensi ? 'required|exists:kompetensi,id_kompetensi' : '',
            'nama_kriteria' => $request->nama_kriteria ? 'required' : '',
            'jenis' => $request->jenis ? 'required|in:benefit,cost' : '',
        ]);
        $data = Kriteria::find($id);
        $data->id_kompetensi = $request->get('id_kompetensi', $data->id_kompetensi);
        $data->kode_kriteria = $request->get('kode_kriteria', $data->kode_kriteria);
        $data->nama_kriteria = $request->get('nama_kriteria', $data->nama_kriteria);
        $data->jenis = $request->get('jenis', $data->jenis);
        $data->save();
        return response()->json(['message' => 'updated'], 200);
    }

    public function resetPrioritas(Request $request)
    {
        foreach ($request->id_kriteria as $key => $value) {
            Kriteria::where(['id_kriteria' => $value])->update(['prioritas' => $key + 1]);
        }
        return response()->json(['message' => 'updated'], 200);
    }
}
