<?php

namespace App\Http\Controllers\Api;

use App\Guru;
use App\Http\Controllers\Controller;
use App\Kriteria;
use App\Nilai;
use App\Transformers\GuruTransformer;
use DataTables;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class NilaiController extends Controller
{
    public function datatable(Request $request)
    {
        $this->validate($request, [
            'id_periode' => 'required|exists:periode,id_periode',
        ]);

        $kriteria = Kriteria::where('id_periode', $request->id_periode)
            ->join('kompetensi', 'kompetensi.id_kompetensi', '=', 'kriteria.id_kompetensi')
            ->get();

        $data = Guru::select('guru.*', 'user.nama_user')
            ->join('user', 'user.id_user', '=', 'guru.id_user')
            ->leftJoin('plotting', 'plotting.id_user', '=', 'user.id_user');
        if ($request->id_penilai) {
            $data = $data->where('plotting.id_penilai', '=', $request->id_penilai);
        }
        $data = $data
            ->where('user.role', '3')
            ->get();

        $datatable = DataTables::of($data);
        foreach ($kriteria as $key => $value) {
            $id_kriteria = $value->id_kriteria;
            $datatable->addColumn(str_replace(' ', '_', strtolower($value->nama_kriteria)), function ($data) use ($id_kriteria) {
                $n = Nilai::where('id_guru', $data->id_guru)
                    ->where('id_kriteria', $id_kriteria)->first();
                return $n ? $n->nilai : '';
            });
        }
        return $datatable->make(true);
    }
    public function datatableGuru(Request $request)
    {
        $this->validate($request, [
            'id_periode' => 'required|exists:periode,id_periode',
            'id_penilai' => 'required|exists:guru,id_guru',
        ]);

        $kriteria = Kriteria::where('id_periode', $request->id_periode)
            ->join('kompetensi', 'kompetensi.id_kompetensi', '=', 'kriteria.id_kompetensi')
            ->get();

        $data = Guru::select('guru.*', 'user.nama_user', 'plotting.*')
            ->leftJoin('plotting', 'plotting.id_guru', '=', 'guru.id_guru')
            ->join('user', 'user.id_user', '=', 'guru.id_user')
            ->where('plotting.id_penilai', '=', $request->id_penilai)
            ->get();

        $datatable = DataTables::of($data);
        foreach ($kriteria as $key => $value) {
            $id_kriteria = $value->id_kriteria;
            $datatable->addColumn(str_replace(' ', '_', strtolower($value->nama_kriteria)), function ($data) use ($id_kriteria) {
                $n = Nilai::where('id_guru', $data->id_guru)
                    ->where('id_kriteria', $id_kriteria)->first();
                return $n ? $n->nilai : '';
            });
        }
        return $datatable->make(true);
    }

    public function get(Request $request)
    {
        $this->validate($request, [
            'id_periode' => 'required|exists:periode,id_periode',
        ]);
        $limit = 10;
        $param = [];
        $data = Guru::orderBy('id_guru', 'desc');
        if ($request->exists('nama_guru')) {
            $data = $data->where('nama_guru', 'LIKE', "%" . $request->nama_guru . "%");
            $param['nama_guru'] = $request->nama_guru;
        }
        if ($request->exists('limit')) {
            $limit = $request->limit;
            $param['limit'] = $request->limit;
        }
        $data = $data->paginate($limit)
            ->appends($param);
        return fractal()
            ->collection($data)
            ->includeNilai()
            ->transformWith(new GuruTransformer($request->id_periode))
            ->paginateWith(new IlluminatePaginatorAdapter($data))
            ->toArray();
    }

    public function getById(Request $request, $id)
    {
        $this->validate($request, [
            'id_periode' => 'required|exists:periode,id_periode',
        ]);
        $data = Guru::select('guru.*', 'user.nama_user')
            ->join('user', 'user.id_user', '=', 'guru.id_user')
            ->find($id);
        return fractal()
            ->item($data)
            ->includeNilai()
            ->transformWith(new GuruTransformer($request->id_periode))
            ->toArray();
    }

    public function put(Request $request, $id)
    {
        $this->validate($request, [
            // 'id_periode' => 'required|exists:periode,id_periode',
            // 'nilai' => 'required|numeric|min:1|max:100',
        ]);

        foreach ($request->id_kriteria as $key => $value) {
            $data = [
                'id_guru' => $id,
                // 'id_periode' => $request->id_periode,
                'id_kriteria' => $request->id_kriteria[$key],
                'nilai' => $request->nilai[$key],
            ];
            // cek nilai eksis
            $cek = Nilai::where('id_guru', $id)
            // ->where('id_periode', $request->id_periode)
                ->where('id_kriteria', $request->id_kriteria[$key])
                ->first();
            if ($cek) {
                Nilai::where('id_guru', $id)
                // ->where('id_periode', $request->id_periode)
                    ->where('id_kriteria', $request->id_kriteria[$key])
                    ->update($data);
            } else {
                Nilai::create($data);
            }
        }
        return response()->json(['message' => 'updated'], 200);
    }
}
