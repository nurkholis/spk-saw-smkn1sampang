<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kompetensi extends Model
{
    protected $table = 'kompetensi';
    protected $primaryKey = 'id_kompetensi';
    public $timestamps = false;
    protected $guarded = [];
}
